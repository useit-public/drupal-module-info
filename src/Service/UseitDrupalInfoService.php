<?php

namespace Drupal\useit_drupal_info\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\State\StateInterface;
use GuzzleHttp\ClientInterface;

class UseitDrupalInfoService {

  /**
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * @param \Drupal\Core\State\StateInterface $state
   *   The state key/value store service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   */
  public function __construct(
    StateInterface $state,
    ConfigFactoryInterface $config_factory,
    ClientInterface $http_client,
    LoggerChannelFactoryInterface $logger_factory
  ) {
    $this->state = $state;
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
    $this->logger = $logger_factory->get('useit_cron');
  }

  /**
   * Envía la información de Drupal y módulos a la URL de destino si corresponde.
   */
  public function checkAndSendData() {
    $last_post_cron = $this->state->get('useit_drupal_info.cron_last') ?? 0;

    $end_settings = $this->configFactory->get('useit_drupal_info.post_destination_settings');
    $cron_interval = $end_settings->get('cron_interval');

    if ((time() - $last_post_cron) <= $cron_interval) {
      return;
    }

    include_once DRUPAL_ROOT . '/core/modules/update/update.compare.inc';

    $available = update_get_available(TRUE);
    $projects = update_calculate_project_data($available);

    if (!isset($projects['drupal'])) {
      return;
    }

    $recommendedKey = $projects['drupal']['recommended'] ?? NULL;
    $recommendedRelease = ($recommendedKey && isset($projects['drupal']['releases'][$recommendedKey]))
      ? $projects['drupal']['releases'][$recommendedKey]
      : NULL;

    $modules = [];
    foreach ($projects as $key => $project) {
      if ($key === 'drupal') {
        continue;
      }
      $modules[] = [
        'name' => $project['name'],
        'version' => $project['existing_version'],
        'recommended' => $project['recommended'],
        'last' => $project['latest_version'],
        'status' => $project['status'],
      ];
    }

    $version = $projects['drupal']['existing_version'] ?? 'N/A';
    $recommendedVersion = $recommendedRelease['version'] ?? $version;
    $status = $projects['drupal']['status'] ?? 0;

    $siteName = $this->configFactory->get('system.site')->get('name');
    $baseUrl = \Drupal::request()->getSchemeAndHttpHost() . \Drupal::request()->getBasePath();

    $data = [
      'version' => $version,
      'recommended_version' => $recommendedVersion,
      'link' => $baseUrl,
      'name' => $siteName,
      'modules' => $modules,
      'status' => $status,
    ];

    $api_url = $end_settings->get('destination_url');

    try {
      $response = $this->httpClient->post($api_url, [
        'json' => $data,
        'headers' => [
          'Content-Type' => 'application/json',
          'Accept'       => 'application/json',
        ],
      ]);

      $responseBody = $response->getBody()->getContents();
      $this->logger->info('Respuesta de la solicitud POST: @response', [
        '@response' => $responseBody,
      ]);
    } catch (\Exception $e) {
      $this->logger->error('Error en la solicitud POST: @error', [
        '@error' => $e->getMessage(),
      ]);
    }

    $this->state->set('useit_drupal_info.cron_last', time());
  }
}
