<?php

namespace Drupal\useit_drupal_info\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configura los ajustes de destino de publicaciones para el sitio.
 */
class PostDestinationSettingsForm extends ConfigFormBase {

  /**
   * El servicio de estado.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * El servicio de formateo de fechas.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * Constructor actualizado para Drupal 11.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   El factory de configuración.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   El servicio de gestión de configuración tipada.
   * @param \Drupal\Core\State\StateInterface $state
   *   El servicio de estado.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   El servicio de formateo de fechas.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typed_config_manager,
    StateInterface $state,
    DateFormatterInterface $date_formatter
  ) {
    parent::__construct($config_factory, $typed_config_manager, 'useit_drupal_info.post_destination_settings');
    $this->state = $state;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('state'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'useit_post_destination_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['useit_drupal_info.post_destination_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('useit_drupal_info.post_destination_settings');
    $last_post_cron = $this->state->get('useit_drupal_info.cron_last');
    $formatted_last_cron = $last_post_cron ?
      $this->dateFormatter->format($last_post_cron, 'custom', 'Y-m-d H:i:s') :
      $this->t('Never');

    $form['destination_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Destination URL'),
      '#description' => $this->t('Destination where the information of this project will be sent from.'),
      '#default_value' => $config->get('destination_url') ?: '',
      '#required' => TRUE,
    ];

    $form['cron_interval'] = [
      '#type' => 'select',
      '#title' => $this->t('Cron interval'),
      '#description' => $this->t('Interval to POST the information of this project. Last post: @cron', ['@cron' => $formatted_last_cron]),
      '#options' => [
        0       => $this->t('Always'),
        10800   => $this->t('Every 3 hours'),
        21600   => $this->t('Every 6 hours'),
        43200   => $this->t('Every 12 hours'),
        86400   => $this->t('Every day'),
        604800  => $this->t('Every week'),
      ],
      '#default_value' => $config->get('cron_interval') ?? 86400,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('useit_drupal_info.post_destination_settings')
      ->set('destination_url', $form_state->getValue('destination_url'))
      ->set('cron_interval', $form_state->getValue('cron_interval'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
